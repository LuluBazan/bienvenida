public class Persona {
    
    private String nombre = "Lourdes";
    private int edad = 20;

    public String decirNombre(){
        return nombre;
    }

    public int decirEdad(){
        return edad;
    }

}