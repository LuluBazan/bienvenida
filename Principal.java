public class Principal {
    public static void main(String[] args) {

        System.out.println("Hola, bienvenido al paradigma Orientado a Objetos");
        
        Saludo saludo = new Saludo();
        Persona persona1 = new Persona();

        String saludo1 = saludo.realizarSaludo();
        String nombre = persona1.decirNombre();
        int edad = persona1.decirEdad();

        System.out.println(saludo1);
        System.out.println("Hola como estas " +nombre+ "?");
        System.out.println("Edad? " +edad);
    }
    
}